from django.contrib import admin
from rangefilter.filter import DateTimeRangeFilter

from . import models



# -----------------------
#	Generic Admin
# -----------------------

class BaseModel_UserDesk_Booking_Admin(admin.ModelAdmin):
	"""Generic Model Admin for the generic model"""

	list_display = ('pk', 'user', 'desk', 'ts_from', 'ts_to',)
	list_filter = (('ts_from', DateTimeRangeFilter), ('ts_to', DateTimeRangeFilter), 'user', 'desk', 'desk__room', 'desk__room__office', 'desk__room__office__company',)
	search_fields = ('user__username', 'user__first_name', 'user__last_name', 'user__email', 'user__slack', 'desk__name',)



# -----------------------
#	Booking Admin
# -----------------------

class Booking_Inline(admin.TabularInline):
	"""Inline to directly view and manage releases of a ownership"""
	model= models.Booking
	extra= 0
	show_change_link = True

class Booking_Admin(BaseModel_UserDesk_Booking_Admin):
	pass



# -----------------------
#	Release Admin
# -----------------------

class Release_Inline(admin.TabularInline):
	"""Inline to directly view and manage releases of a ownership"""
	model= models.Release
	extra= 0
	show_change_link = True

class Release_Admin(admin.ModelAdmin):

	list_display = ('pk', 'get_user', 'get_desk', 'get_ownership_ts_from', 'get_ownership_ts_to', 'ts_from', 'ts_to',)
	list_filter = (('ts_from', DateTimeRangeFilter), ('ts_to', DateTimeRangeFilter), 'ownership__user', 'ownership__desk', 'ownership__desk__room__office__company',)
	search_fields = ('ownership__user__username', 'ownership__user__first_name', 'ownership__user__last_name', 'ownership__user__email', 'ownership__user__slack', 'ownership__desk__name',)

	def get_user(self, obj):
		return obj.ownership.user
	get_user.short_description = 'User'
	get_user.admin_order_field = 'ownership__user'

	def get_desk(self, obj):
		return obj.ownership.desk
	get_desk.short_description = 'Desk'
	get_desk.admin_order_field = 'ownership__desk'

	def get_ownership_ts_from(self, obj):
		return obj.ownership.ts_from
	get_ownership_ts_from.short_description = 'Ownership ts_from'
	get_ownership_ts_from.admin_order_field = 'ownership__ts_from'

	def get_ownership_ts_to(self, obj):
		return obj.ownership.ts_to
	get_ownership_ts_to.short_description = 'Ownership ts_to'
	get_ownership_ts_to.admin_order_field = 'ownership__ts_to'



# -----------------------
#	Ownership Admin
# -----------------------

class Ownership_Inline(admin.TabularInline):
	"""Inline to directly view and manage ownership of desk"""
	model= models.Ownership
	extra= 0
	show_change_link = True

class Ownership_Admin(BaseModel_UserDesk_Booking_Admin):
	inlines = [
		Release_Inline,
	]



# -----------------------
#	Available Desks View Admin
# -----------------------

class View_Available_Desks_Admin(admin.ModelAdmin):

	list_display = ('pk', 'desk', 'ts_free_from', 'ts_free_to',)
	list_filter = (('ts_free_from', DateTimeRangeFilter), ('ts_free_to', DateTimeRangeFilter), 'desk', 'desk__room', 'desk__room__office', 'desk__room__office__company',)
	search_fields = ('desk__name',)



# -----------------------
#	Free Desks View Admin
# -----------------------

class View_Free_Desks_Admin(admin.ModelAdmin):

	list_display = ('pk', 'desk', 'ts_free_from', 'ts_free_to',)
	list_filter = (('ts_free_from', DateTimeRangeFilter), ('ts_free_to', DateTimeRangeFilter), 'desk', 'desk__room', 'desk__room__office', 'desk__room__office__company',)
	search_fields = ('desk__name',)



admin.site.register(models.Ownership, Ownership_Admin)
admin.site.register(models.Release, Release_Admin)
admin.site.register(models.View_Available_Desks, View_Available_Desks_Admin)
admin.site.register(models.Booking, Booking_Admin)
admin.site.register(models.View_Free_Desks, View_Free_Desks_Admin)