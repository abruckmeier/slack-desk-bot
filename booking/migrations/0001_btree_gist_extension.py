from django.db import migrations
from django.contrib.postgres.operations import BtreeGistExtension



class Migration(migrations.Migration):
	"""
	Migration to install extension in PostgreSQL database: Btree Gist for indexing datetimes and timeranges.
	"""

	initial = False
	dependencies = [
		('office', '0003_auto_20201122_1942'),
	]

	operations = [
		BtreeGistExtension(),
	]