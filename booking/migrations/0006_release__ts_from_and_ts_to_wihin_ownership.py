from django.db import migrations



class Migration(migrations.Migration):
	"""

	"""

	initial = False
	dependencies = [
		('booking', '0005_auto_20201228_2106'),
	]

	atomic = False

	operations = [
		migrations.RunSQL(
			sql='''
				create function booking_release_range_is_within_ownership_range()
					returns trigger as
				$$
					declare
						is_within_ownership_range bool;
					begin
						is_within_ownership_range := (
							select tstzrange(ts_from, ts_to) @> tstzrange(NEW.ts_from, NEW.ts_to) as is_within_ownership_range
							from booking_ownership bo
							where bo.id = NEW.ownership_id
						);
						if (is_within_ownership_range is FALSE) then
							raise exception 'booking_release_range_is_within_ownership_range: [ts_from, ts_to) not within ownership range';
						end if;
						return NEW;
					end;
				$$ language plpgsql;
				
				create trigger booking_release_range_is_within_ownership_range
					before insert or update
					on booking_release
					for each row
				execute procedure booking_release_range_is_within_ownership_range();
				
				
				create function booking_ownership_range_is_within_release_range()
					returns trigger as
				$$
					declare
						is_within_release_range bool;
					begin
						is_within_release_range := (
							select count(*)=0 or tstzrange(NEW.ts_from, NEW.ts_to) @> tstzrange(min(br.ts_from), max(br.ts_to)) as within_release_range
							from booking_release br
							where br.ownership_id = NEW.id
						);
						if (is_within_release_range is FALSE) then
							raise exception 'booking_ownership_range_is_within_release_range: [ts_from, ts_to) not within release range';
						end if;
						return NEW;
					end;
				$$ language plpgsql;
				
				create trigger booking_ownership_range_is_within_release_range
					before insert or update
					on booking_ownership
					for each row
				execute procedure booking_ownership_range_is_within_release_range();
			''',
			reverse_sql='''
				drop trigger if exists booking_release_range_is_within_ownership_range on booking_release;
				drop function booking_release_range_is_within_ownership_range();
				drop trigger if exists booking_ownership_range_is_within_release_range on booking_ownership;
				drop function booking_ownership_range_is_within_release_range();
			'''
		),
	]