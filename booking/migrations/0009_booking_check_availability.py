from django.db import migrations



class Migration(migrations.Migration):
	"""

	"""

	initial = False
	dependencies = [
		('booking', '0008_auto_20201230_2115'),
	]

	atomic = False

	operations = [
		migrations.RunSQL(
			sql='''
				create function booking_booking_check_availability()
					returns trigger as
				$$
					declare
						num_availabilities int;
					begin
						num_availabilities := (
							select count(*) as num_avail
							from booking_view_available_desks
							where desk_id = NEW.desk_id
							  and tstzrange(ts_free_from, ts_free_to) @> tstzrange( NEW.ts_from, NEW.ts_to )
						);
						if (num_availabilities < 1) then
							raise exception 'booking_booking_check_availability: [ts_from, ts_to) for desk not available';
						end if;
						return NEW;
					end;
				$$ language plpgsql;
				
				create trigger booking_booking_check_availability
					before insert or update
					on booking_booking
					for each row
				execute procedure booking_booking_check_availability();
			''',
			reverse_sql='''
				drop trigger if exists booking_booking_check_availability on booking_booking;
				drop function booking_booking_check_availability();
			'''
		),
	]