from django.db import migrations



class Migration(migrations.Migration):
	"""

	"""

	initial = False
	dependencies = [
		('booking', '0009_booking_check_availability'),
	]

	atomic = False

	operations = [
		migrations.RunSQL(
			sql='''
				create function modify_release__no_booking_interference()
					returns trigger as
				$$
					declare
						is_allowed bool;
					begin
						is_allowed := (
							select bool_and( tstzrange(bb.ts_from,bb.ts_to) <@ tstzrange(NEW.ts_from, NEW.ts_to) )
							from booking_release br
							join booking_ownership bo
								on br.ownership_id = bo.id
							join booking_booking bb
								on tstzrange(bb.ts_from,bb.ts_to) <@ tstzrange(br.ts_from,br.ts_to)
								  and bo.desk_id = bb.desk_id
							where br.id = NEW.id
						);
						if (is_allowed is false) then
							raise exception 'modify_release__no_booking_interference: Changing the release to [ts_from, ts_to) would interfere with a already made booking';
						end if;
						return NEW;
					end;
				$$ language plpgsql;
				
				create trigger modify_release__no_booking_interference
					before update
					on booking_release
					for each row
				execute procedure modify_release__no_booking_interference();
			''',
			reverse_sql='''
				drop trigger if exists modify_release__no_booking_interference on booking_release;
				drop function modify_release__no_booking_interference();
			'''
		),
	]