from django.db import migrations



class Migration(migrations.Migration):
	"""

	"""

	initial = False
	dependencies = [
		('booking', '0010_modify_release__no_booking_interference'),
	]

	atomic = False

	operations = [
		migrations.RunSQL(
			sql='''
				create function ownership__no_booking_interference()
					returns trigger as
				$$
					declare
						ownership_touches_booking bool;
					begin
						ownership_touches_booking := (
							with bookings_within_releases as (
								select bb.*
								from booking_release br
								join booking_ownership bo
									on br.ownership_id = bo.id
								join booking_booking bb
									on tstzrange(bb.ts_from,bb.ts_to) <@ tstzrange(br.ts_from,br.ts_to)
									 and bo.desk_id = bb.desk_id
							)
							select bool_or( tstzrange(bb.ts_from,bb.ts_to) && tstzrange(NEW.ts_from, NEW.ts_to) ) as ownership_touches_booking
							from (
								select * from booking_booking
								except
								select * from bookings_within_releases
							) bb
							where bb.desk_id = 1
						);
						if (ownership_touches_booking is true) then
							raise exception 'ownership__no_booking_interference: Changing/Creating the ownership of desk to [ts_from, ts_to) would interfere with a already made booking';
						end if;
						return NEW;
					end;
				$$ language plpgsql;
				
				create trigger ownership__no_booking_interference
					before insert or update
					on booking_ownership
					for each row
				execute procedure ownership__no_booking_interference();
			''',
			reverse_sql='''
				drop trigger if exists ownership__no_booking_interference on booking_ownership;
				drop function ownership__no_booking_interference();
			'''
		),
	]