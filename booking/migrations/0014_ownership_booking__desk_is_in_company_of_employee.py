from django.db import migrations



class Migration(migrations.Migration):
	"""

	"""

	initial = False
	dependencies = [
		('booking', '0013_auto_20210106_1902'),
	]

	atomic = False

	operations = [
		migrations.RunSQL(
			sql='''
				create function ownership_booking__desk_is_in_company_of_employee()
					returns trigger as
				$$
					declare
						employee_company int;
						desk_company int;
					begin
						employee_company := (
							select company_id
							from office_employee
							where id = NEW.user_id
						);
						desk_company := (
							select oo.company_id
							from office_desk od
							join office_room o on od.room_id = o.id
							join office_office oo on o.office_id = oo.id
							where od.id = NEW.desk_id
						);
						if (employee_company != desk_company) then
							raise exception 'ownership_booking__desk_is_in_company_of_employee: Desk is not in company, user is employee of.';
						end if;
						return NEW;
					end;
				$$ language plpgsql;
				
				create trigger ownership__desk_is_in_company_of_employee
					before insert or update
					on booking_ownership
					for each row
				execute procedure ownership_booking__desk_is_in_company_of_employee();
				
				create trigger booking__desk_is_in_company_of_employee
					before insert or update
					on booking_booking
					for each row
				execute procedure ownership_booking__desk_is_in_company_of_employee();
			''',
			reverse_sql='''
				drop trigger if exists ownership__desk_is_in_company_of_employee on booking_ownership;
				drop trigger if exists booking__desk_is_in_company_of_employee on booking_booking;
				drop function ownership_booking__desk_is_in_company_of_employee();
			'''
		),
	]