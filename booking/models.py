from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.apps import apps as django_apps
from django.utils import timezone
from django.conf import settings
import django_helpers.validate_unique
from django.contrib.postgres.constraints import ExclusionConstraint
from django.contrib.postgres.fields import (
	DateTimeRangeField,
	RangeBoundary,
	RangeOperators,
)
from django.contrib.postgres.functions import TransactionNow
from django.core.exceptions import ValidationError
from django.core.exceptions import NON_FIELD_ERRORS
from django_db_views.db_view import DBView



"""This app connects user and desk to each other with a booking system. `User` and `Desk` are present in other apps. 
 	The models to use can be adjusted in settings under `BOOKING.DESK_MODEL` and `BOOKING.EMPLOYEE_MODEL`.
"""
DeskModel = django_apps.get_model(settings.BOOKING.DESK_MODEL, require_ready= False)
EmployeeModel = django_apps.get_model(settings.BOOKING.EMPLOYEE_MODEL, require_ready= False)



class TsTzRange(models.Func):
	"""
	From two datetimes, create a datetimerange.
	For exclude constraints in `Owner`
	"""
	function = 'TSTZRANGE'
	output_field = DateTimeRangeField()



def _datetime_next_full_hour_from_one_minute_on():
	""" From now, round up to the next full minute and than to the next full hour.
	This will be the default for the `ts_from`: The next full hour, but with one minute buffer, so, the full hour will not be past e.g. at 10:59:59.

	:return: datetime instance
	"""
	new_default = timezone.now().replace(second=0, microsecond=0) + timezone.timedelta(minutes=1)
	return new_default.replace(minute=0) + timezone.timedelta(hours=1)



def _datetime_beginning_of_next_day_from_one_minute_on():
	""" From now, round up to the beginning of the next day.
	This will be the default for the `ts_to`: The next day, but with one minute buffer, so, the next day will not be past e.g. at 23:59:59.

	:return: datetime instance
	"""
	new_default = timezone.now().replace(second=0, microsecond=0) + timezone.timedelta(minutes=1)
	return new_default.replace(hour=0, minute=0) + timezone.timedelta(days=1)



class BaseModel_UserDesk_Booking(models.Model):
	"""Base model for Ownership and booking models. Connection between user and desk models with booking time range
	"""

	created = models.DateTimeField(auto_now_add=True)
	modified = models.DateTimeField(auto_now=True)

	user = models.ForeignKey(
		EmployeeModel,
		on_delete= models.CASCADE,
		#related_name= 'user',
	)
	desk = models.ForeignKey(
		DeskModel,
		on_delete= models.CASCADE,
		#related_name= 'desk',
	)
	ts_from = models.DateTimeField(
		default= _datetime_next_full_hour_from_one_minute_on,
		help_text= 'Timestamp, the timerange starts.',
	)
	ts_to = models.DateTimeField(
		blank= True, null= True,
		help_text= 'Timestamp, the timerange ends. If not set, the timerange is ongoing.',
	)

	class Meta:
		abstract = True

		constraints = [

			models.CheckConstraint(
				check= models.Q(
					ts_from__lt= models.F('ts_to'),
				),
				name= '%(app_label)s_%(class)s__ts_from__lower_than__ts_to',
			),

			models.UniqueConstraint(
				fields= ['user',],
				condition= models.Q(
					ts_to__isnull= True,
				),
				name= '%(app_label)s_%(class)s__user_must_end_timerange_before_new',
			),

			models.UniqueConstraint(
				fields= ['desk',],
				condition= models.Q(
					ts_to__isnull= True,
				),
				name= '%(app_label)s_%(class)s__desk_timerange_must_end_before_new',
			),

			ExclusionConstraint(
				name= '%(app_label)s_%(class)s__exclude_overlapping_user_timerange_of_desk',
				expressions= (
					(
						TsTzRange('ts_from', 'ts_to', RangeBoundary(inclusive_lower= True, inclusive_upper= False)),
						RangeOperators.OVERLAPS,
					),
					('desk', RangeOperators.EQUAL),
				),
				condition= None,
			),

			ExclusionConstraint(
				name= '%(app_label)s_%(class)s__exclude_overlapping_user_timerange',
				expressions= (
					(
						TsTzRange('ts_from', 'ts_to', RangeBoundary(inclusive_lower= True, inclusive_upper= False)),
						RangeOperators.OVERLAPS,
					),
					('user', RangeOperators.EQUAL),
				),
				condition= None,
			),

		]


	def validate_unique(self, exclude=None):
		super(BaseModel_UserDesk_Booking, self).validate_unique(exclude)
		if not all( [getattr(self, _field, None) for _field in ('user','desk','ts_from')] ):
			return

		# user_must_end_timerange_before_new
		django_helpers.validate_unique.validate_unique(
			self,
			user= self.user,
			ts_to__isnull= True,
		)

		# desk_timerange_must_end_before_new
		django_helpers.validate_unique.validate_unique(
			self,
			desk= self.desk,
			ts_to__isnull= True,
		)

		# exclude_timerange_user_ownership_of_desk
		if not self.ts_to or self.ts_to > self.ts_from:
			data = self.__class__.objects.raw(
				"""
				select id
				from {table}
				where desk_id = %(desk_id)s
				  and ts_to is not NULL
				  and ts_to >= %(ts_from_new)s
				  and tstzrange(ts_from, ts_to, '[)')
					  && tstzrange( %(ts_from_new)s, %(ts_to_new)s, '[)' )
				""".format(
					table= self.__class__._meta.db_table,
				),
				dict(
					desk_id= self.desk_id,
					ts_from_new= self.ts_from,
					ts_to_new= self.ts_to,
				),
			)
			if self.id:
				data = self.__class__.objects.filter(
					id__in= tuple([d.id for d in data]),
				)
				data = data.exclude(id=self.id)
			if data:
				raise ValidationError({
						NON_FIELD_ERRORS: [
							_(f'There exists a overlapping timerange of a desk with ID {data[0].id}.'),
						],
				})

		# exclude_overlapping_user_timerange
		if not self.ts_to or self.ts_to > self.ts_from:
			data = self.__class__.objects.raw(
				"""
				select id
				from {table}
				where user_id = %(user_id)s
				  and ts_to is not NULL
				  and ts_to >= %(ts_from_new)s
				  and tstzrange(ts_from, ts_to, '[)')
					  && tstzrange( %(ts_from_new)s, %(ts_to_new)s, '[)' )
				""".format(
					table= self.__class__._meta.db_table,
				),
				dict(
					user_id= self.user_id,
					ts_from_new= self.ts_from,
					ts_to_new= self.ts_to,
				),
			)
			if self.id:
				data = self.__class__.objects.filter(
					id__in= tuple([d.id for d in data]),
				)
				data = data.exclude(id=self.id)
			if data:
				raise ValidationError({
						NON_FIELD_ERRORS: [
							_(f'There exists a overlapping timerange of a user with ID {data[0].id}.'),
						],
				})


	def clean(self):
		super(BaseModel_UserDesk_Booking, self).clean()
		if not all( [getattr(self, _field, None) for _field in ('user','desk','ts_from')] ):
			return

		# ts_from__lower_than__ts_to
		if self.ts_to:
			if self.ts_from >= self.ts_to:
				raise ValidationError('ts_from must be lower than ts_to')

		# desk_is_in_company_of_employee
		if self.user.company_id != self.desk.room.office.company_id:
			raise ValidationError('desk is not within company, the user is employee of')


	def __str__(self):
		strformat = '%d.%m.%Y %H:%M:%S %z'
		return f"({self.id}) [{self.user}] {self.desk}: {self.ts_from.strftime(strformat)} -{f' {self.ts_to.strftime(strformat)}' if self.ts_to else ''}"



class Ownership(BaseModel_UserDesk_Booking):
	""" Ownership of a desk. The owner can have privileges for this desk and can revoke a booking for his/her desk.
	"""
	class Meta(BaseModel_UserDesk_Booking.Meta):
		abstract = False


	def clean(self):
		super(Ownership, self).clean()
		if not all( [getattr(self, _field, None) for _field in ('user','desk','ts_from')] ):
			return

		# ts_from__lte__min_release_start
		min_release_ts_from= self.release_set.all().aggregate(models.Min('ts_from')).get('ts_from__min')
		if min_release_ts_from and self.ts_from > min_release_ts_from:
			raise ValidationError('ts_from must not be greater than lowest ts_from of releases')

		# ts_to__gte__max_release_end
		if self.ts_to:
			max_release_ts_to= self.release_set.all().aggregate(models.Max('ts_to')).get('ts_to__max')
			if max_release_ts_to and self.ts_to < max_release_ts_to:
				raise ValidationError('ts_to must not be lower than greatest ts_to of releases')

		# ownership__no_booking_interference
		touches_booking = self.__class__.objects.raw(
			"""
			with bookings_within_releases as (
				select bb.*
				from {release_table} br
				join {ownership_table} bo
					on br.ownership_id = bo.id
				join {booking_table} bb
					on tstzrange(bb.ts_from,bb.ts_to) <@ tstzrange(br.ts_from,br.ts_to)
					 and bo.desk_id = bb.desk_id
			)		
			select NULL::int as id,
				   bool_or( tstzrange(bb.ts_from,bb.ts_to) && tstzrange( %(ts_from_new)s , %(ts_to_new)s ) ) as touches_booking
			from (
				select * from {booking_table}
				except
				select * from bookings_within_releases
			) bb
			where bb.desk_id = %(self_desk_id)s
			group by id
			""".format(
				release_table= Release._meta.db_table,
				ownership_table= self.__class__._meta.db_table,
				booking_table= Booking._meta.db_table,
			),
			dict(
				self_desk_id= self.desk_id,
				ts_from_new= self.ts_from,
				ts_to_new= self.ts_to,
			),
		)
		if touches_booking:
			if touches_booking[0].touches_booking:
				raise ValidationError('A made booking forbids the insertion or modification of the ownership of the desk with the time range.')



class Release(models.Model):
	""" The owner of a desk can release the desk for booking within the time range of ownership
	"""

	created = models.DateTimeField(auto_now_add=True)
	modified = models.DateTimeField(auto_now=True)

	ownership = models.ForeignKey(
		Ownership,
		on_delete= models.CASCADE,
		#related_name= 'ownership',
	)
	ts_from = models.DateTimeField(
		default= _datetime_next_full_hour_from_one_minute_on,
		help_text= 'Timestamp, the timerange starts.',
	)
	ts_to = models.DateTimeField(
		default= _datetime_beginning_of_next_day_from_one_minute_on,
		help_text= 'Timestamp, the timerange ends.',
	)


	class Meta:
		constraints = [

			models.CheckConstraint(
				check= models.Q(
					ts_from__lt= models.F('ts_to'),
				),
				name= '%(app_label)s_%(class)s__ts_from__lower_than__ts_to',
			),

			ExclusionConstraint(
				name= '%(app_label)s_%(class)s__exclude_overlapping_timerange',
				expressions= (
					(
						TsTzRange('ts_from', 'ts_to', RangeBoundary(inclusive_lower= True, inclusive_upper= False)),
						RangeOperators.OVERLAPS,
					),
					('ownership', RangeOperators.EQUAL),
				),
				condition= None,
			),

		]


	def validate_unique(self, exclude=None):
		super(Release, self).validate_unique(exclude)
		if not all( [getattr(self, _field, None) for _field in ('ownership','ts_from','ts_to')] ):
			return

		# exclude_overlapping_timerange
		if self.ts_to > self.ts_from:
			data = self.__class__.objects.raw(
				"""
				select id
				from {table}
				where ownership_id = %(ownership_id)s
				  and ts_to is not NULL
				  and ts_to >= %(ts_from_new)s
				  and tstzrange(ts_from, ts_to, '[)')
					  && tstzrange( %(ts_from_new)s, %(ts_to_new)s, '[)' )
				""".format(
					table= self.__class__._meta.db_table,
				),
				dict(
					ownership_id= self.ownership_id,
					ts_from_new= self.ts_from,
					ts_to_new= self.ts_to,
				),
			)
			if self.id:
				data = self.__class__.objects.filter(
					id__in= tuple([d.id for d in data]),
				)
				data = data.exclude(id=self.id)
			if data:
				raise ValidationError({
						NON_FIELD_ERRORS: [
							_(f'There exists a overlapping timerange of a release with ID {data[0].id}.'),
						],
				})


	def clean(self):
		super(Release, self).clean()
		if not all( [getattr(self, _field, None) for _field in ('ownership','ts_from','ts_to')] ):
			return

		# ts_from__gte__ownership_start
		if self.ts_from < self.ownership.ts_from:
			raise ValidationError('ts_from must not be lower than ts_from of ownership')

		# ts_to__lte__ownership_end
		if self.ownership.ts_to and self.ts_to > self.ownership.ts_to:
			raise ValidationError('ts_to must not be greater than ts_to of ownership')

		# ts_from__lower_than__ts_to
		if self.ts_from >= self.ts_to:
			raise ValidationError('ts_from must be lower than ts_to')

		# modify_release__no_booking_interference
		if self.id:
			is_allowed = self.__class__.objects.raw(
				"""
				select br.id,
					   bool_and( tstzrange(bb.ts_from,bb.ts_to) <@ tstzrange( %(ts_from_new)s , %(ts_to_new)s ) ) as is_allowed
				from {release_table} br
				join {ownership_table} bo
					on br.ownership_id = bo.id
				join {booking_table} bb
					on tstzrange(bb.ts_from,bb.ts_to) <@ tstzrange(br.ts_from,br.ts_to)
					  and bo.desk_id = bb.desk_id
				where br.id = %(release_id)s
				group by br.id
				""".format(
					release_table= self.__class__._meta.db_table,
					ownership_table= Ownership._meta.db_table,
					booking_table= Booking._meta.db_table,
				),
				dict(
					release_id= self.id,
					ts_from_new= self.ts_from,
					ts_to_new= self.ts_to,
				),
			)
			if is_allowed:
				if not is_allowed[0].is_allowed:
					raise ValidationError('A made booking forbids the modification of the release range to the new value.')


	def __str__(self):
		strformat = '%d.%m.%Y %H:%M:%S %z'
		return f"({self.id}) {self.ownership}, on from {self.ts_from.strftime(strformat)} - {self.ts_to.strftime(strformat)}"



class View_Available_Desks(DBView):
	"""
	View of available desks before booking:
	- released slots of owned desks
	- desks, currently not in ownership
	- newer owned desks
	"""

	desk = models.ForeignKey(
		DeskModel,
		on_delete= models.DO_NOTHING,
		#related_name= 'desk',
	)
	ts_free_from = models.DateTimeField(
		null= True, blank= True,
		help_text= 'Timestamp, the free timerange starts.',
	)
	ts_free_to = models.DateTimeField(
		null= True, blank= True,
		help_text= 'Timestamp, the free timerange ends.',
	)

	view_definition = """
		with free_desk_slots as (
		-- released slots
			select od.id      as desk_id,
				   br.ts_from as free_from,
				   br.ts_to   as free_to
			from office_desk od
					 join booking_ownership bo on od.id = bo.desk_id
					 join booking_release br on bo.id = br.ownership_id
		
			union all
		
		-- not currently in ownership
			select *
			from (
					 select desk_id,
							ts_to 		 as free_from,
							lead(ts_from, 1) over (partition by desk_id order by ts_from) as free_to
					 from booking_ownership
				 ) ua
			where free_from is not NULL
			and (free_from != free_to or free_to is NULL)
		
			union all
		
		-- not currently in ownership - before first ownership (not in first part)
			select desk_id,
				   NULL::timestamptz(0) as before_first_ownership,
				   min(ts_from)      as start_first_os
			from booking_ownership
			group by desk_id
		
			union all
		
		-- newer owned
			select desk_id,
				   NULL::timestamptz(0) as free_from,
				   NULL::timestamptz(0) as free_to
			from (
					 select id as desk_id
					 from office_desk
						 except
					 select distinct desk_id
					 from booking_ownership
				 ) ua
		)
		select row_number() over () as id,
			   desk_id,
			   free_from as ts_free_from,
			   free_to as ts_free_to
		from free_desk_slots
		order by desk_id, free_to desc
	"""

	class Meta:
		managed= False
		db_table= 'booking_view_available_desks'
		verbose_name = 'View: Available Desk'
		verbose_name_plural = 'View: Available Desks'

	def __str__(self):
		strformat = '%d.%m.%Y %H:%M:%S %z'
		return f'Desk ID {self.desk_id}: [ {self.ts_free_from.strftime(strformat)} , {self.ts_free_to.strftime(strformat)} )'



class Booking(BaseModel_UserDesk_Booking):
	"""
	Booking of a desk. Either released desk or a not owned desk.
	ts_to cannot be Null. Validation and database checks for that are not affected.
	"""

	ts_to = models.DateTimeField(
		help_text= 'Timestamp, the timerange ends.',
	)


	def clean(self):
		super(Booking, self).clean()
		if not all( [getattr(self, _field, None) for _field in ('user','desk','ts_from','ts_to')] ):
			return

		# check for availabiliy
		available_desks = View_Available_Desks.objects.raw(
			"""
			select id
			from {table}
			where desk_id = %(self_desk_id)s
			  and tstzrange(ts_free_from, ts_free_to) @> tstzrange( %(self_ts_from)s, %(self_ts_to)s )
			""".format(
				table= View_Available_Desks._meta.db_table,
			),
			dict(
				self_desk_id= self.desk_id,
				self_ts_from= self.ts_from,
				self_ts_to= self.ts_to,
			),
		)
		if not available_desks:
			raise ValidationError('This desk is not available for the given time range.')



class View_Free_Desks(DBView):
	"""
	View of free desks
	"""

	desk = models.ForeignKey(
		DeskModel,
		on_delete= models.DO_NOTHING,
		#related_name= 'desk',
	)
	ts_free_from = models.DateTimeField(
		null= True, blank= True,
		help_text= 'Timestamp, the free timerange starts.',
	)
	ts_free_to = models.DateTimeField(
		null= True, blank= True,
		help_text= 'Timestamp, the free timerange ends.',
	)

	view_definition = """
		with updated_free_slots as (
			/*
			 Prerequisite: Booking ts range must be fully within a available ts range slot!
			 Blend available desks and booking data.
			 */
			select ad.id,
				   ad.desk_id,
				   unnest(array [ts_free_from, ts_to]) as ts_free_from,
				   unnest(array [ts_from, ts_free_to]) as ts_free_to
			from booking_view_available_desks ad
					 join booking_booking bb
						  on tstzrange(ad.ts_free_from, ad.ts_free_to) @>
							 tstzrange(bb.ts_from, bb.ts_to)
							  and ad.desk_id = bb.desk_id
		)
		
		select row_number() over () as id,
			   *
		from (
				 select desk_id,
						ts_free_from,
						ts_free_to
				 from updated_free_slots
		
				 union all
		
				 select desk_id,
						ts_free_from,
						ts_free_to
				 from booking_view_available_desks
				 where id not in (select distinct id from updated_free_slots)
			 ) ua
	"""

	class Meta:
		managed= False
		db_table= 'booking_view_free_desks'
		verbose_name = 'View: Free Desk'
		verbose_name_plural = 'View: Free Desks'

	def __str__(self):
		strformat = '%d.%m.%Y %H:%M:%S %z'
		return f'Desk ID {self.desk_id}: [ {self.ts_free_from.strftime(strformat)} , {self.ts_free_to.strftime(strformat)} )'
