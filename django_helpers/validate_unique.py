

from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import NON_FIELD_ERRORS



def validate_unique(class_instance, **kwargs_for_filter):
	""" Check for a duplicate in the database, based on the filter settings

	:param class_instance: class instance of django.db.models (or children). Usually called with `self`.
	:param kwargs_for_filter: Key-value arguments passed to the `objects.filter()` function.
	:return: None
	:exception: Validation error if duplicate exists
	"""

	if not class_instance.id:
		if class_instance.__class__.objects.filter(**kwargs_for_filter).exists():
			dupl = class_instance.__class__.objects.filter(**kwargs_for_filter)
			duplicate_ids = set([str(x.id) for x in dupl])
			duplicate_ids = ', '.join(duplicate_ids)
			raise ValidationError({
					NON_FIELD_ERRORS: [
						_(f'There exit duplicates under ID {duplicate_ids}.'),
					],
			})



if __name__=='__main__':

	class MyModel(models.Model):

		# ...

		def validate_unique(self, exclude=None):
			super(MyModel, self).validate_unique(exclude)

			# Check for unique serial number within the company
			django_helpers.validate_unique.validate_unique(
				self,
				company_serial_number= self.company_serial_number,
				desk__room__office__company= self.desk.room.office.company,
			)
