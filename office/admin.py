from django.contrib import admin
from django.conf import settings
import importlib

from . import models

# Import Admin Inlines from booking app, if set in settings
if settings.OFFICE.BOOKING_ADMIN:
	booking_admin_module = importlib.import_module(settings.OFFICE.BOOKING_ADMIN)
else:
	booking_admin_module = None


# -----------------------
# Inlines
# -----------------------

class Employee_Inline(admin.TabularInline):
	model= models.Employee
	extra= 0
	show_change_link = True

class Office_Inline(admin.TabularInline):
	model= models.Office
	extra= 0
	show_change_link = True

class Room_Inline(admin.TabularInline):
	model= models.Room
	extra= 0
	show_change_link = True

class Desk_Inline(admin.TabularInline):
	model= models.Desk
	extra= 0
	show_change_link = True



# -----------------------
#	Company Admin
# -----------------------

class Company_Admin(admin.ModelAdmin):

	inlines = [Office_Inline, Employee_Inline, ]

	list_display = ('pk', 'name',)
	search_fields = ('name',)



# -----------------------
#	Office Admin
# -----------------------

class Office_Admin(admin.ModelAdmin):

	inlines = [Room_Inline,]

	list_display = ('pk', 'name', 'company')
	list_filter = ('company',)
	search_fields = ('name', 'company__name',)



# -----------------------
#	Room Admin
# -----------------------

class Room_Admin(admin.ModelAdmin):

	inlines = [Desk_Inline,]

	list_display = ('pk', 'name', 'office',)
	list_filter = ('office', 'office__company',)
	search_fields = ('name', 'office__name',)



# -----------------------
#	Desk Admin
# -----------------------

class Device_Inline(admin.TabularInline):
	"""Inline to directly manage devices within the view of desk"""
	model= models.Desk.devices.through
	extra= 0
	show_change_link = True

class Desk_Admin(admin.ModelAdmin):
	"""Admin View for desk
	Embed inline element of device
	"""
	inlines = [
		Device_Inline,
	]

	def __init__(self, *args, **kwargs):

		# Load Admin Inlines, if existing
		if booking_admin_module and 'Booking_Inline' in dir(booking_admin_module):
			self.inlines.append(
				booking_admin_module.Booking_Inline
			)
		if booking_admin_module and 'Ownership_Inline' in dir(booking_admin_module):
			self.inlines.append(
				booking_admin_module.Ownership_Inline
			)

		super(Desk_Admin, self).__init__(*args, *kwargs)

	list_display = ('pk', 'name', 'room',)
	list_filter = ('room__office__company', 'room__office', 'room',)
	search_fields = ('name', 'room__name',)



# -----------------------
#	Hardware Admin
# -----------------------

class Hardware_Admin(admin.ModelAdmin):

	list_display = ('pk', 'type', 'brand', 'model', 'main_characteristic',)
	list_filter = ('type', 'brand',)
	search_fields = ('type', 'brand', 'model', 'main_characteristic', 'details',)



# -----------------------
#	Employee Admin
# -----------------------
class Employee_Admin(admin.ModelAdmin):

	list_display = ('pk', 'company', 'user', 'slack',)
	list_filter = ('company',)
	search_fields = ('user', 'slack',)



# -----------------------
#	Register your models here.
# -----------------------
admin.site.register(models.Company, Company_Admin)
admin.site.register(models.Office, Office_Admin)
admin.site.register(models.Room, Room_Admin)
admin.site.register(models.Desk, Desk_Admin)
admin.site.register(models.Hardware, Hardware_Admin)
admin.site.register(models.Employee, Employee_Admin)

