from django.db import migrations



class Migration(migrations.Migration):
	"""
	Migration to
	"""

	initial = False
	dependencies = [
		('office', '0003_auto_20201122_1942'),
	]

	atomic = False

	operations = [
		migrations.RunSQL(
			sql='''
				create function unique_serial_number_within_company()
					returns trigger as
				$$
					declare
						sn_count int;
						var_company_id int;
					begin
						var_company_id := (
							select oo.company_id
							from office_desk od
							join office_room r on od.room_id = r.id
							join office_office oo on r.office_id = oo.id
							where od.id = NEW.desk_id
						);
						sn_count := (
							select count(*) as cnt
							from office_device dev
									 join office_desk od on dev.desk_id = od.id
									 join office_room o on o.id = od.room_id
									 join office_office oo on o.office_id = oo.id
							where dev.company_serial_number = NEW.company_serial_number
							  and oo.company_id = var_company_id
						);
						if (sn_count != 0) then
							raise exception 'unique_serial_number_within_company: company serial number "%" for company "%" is already taken', NEW.company_serial_number, (select name from office_company where id=var_company_id);
						end if;
						return NEW;
					end;
				$$ language plpgsql;
				
				create trigger unique_serial_number_within_company
					before insert or update
					on office_device
					for each row
				execute procedure unique_serial_number_within_company();
			''',
			reverse_sql='''
				drop trigger if exists unique_serial_number_within_company on office_device;
				
				drop function unique_serial_number_within_company();
			'''
		),
	]