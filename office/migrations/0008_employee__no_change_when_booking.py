from django.db import migrations



class Migration(migrations.Migration):
	"""
	Migration to
	"""

	initial = False
	dependencies = [
		('office', '0007_company_slack_token'),
	]

	atomic = False

	operations = [
		migrations.RunSQL(
			sql='''
				create function employee__no_change_when_booking()
					returns trigger as
				$$
					declare
						count_entries int;
					begin
						if (OLD.user_id = NEW.user_id and OLD.company_id = NEW.company_id) then
							return NEW;
						end if;

						count_entries := (
						    select count(*) as cnt_entries
							from (
								select desk_id, user_id from booking_ownership
								union all
								select desk_id, user_id from booking_booking
							) bo_bb
							join office_desk od on bo_bb.desk_id = od.id
							join office_room o on od.room_id = o.id
							join office_office oo on o.office_id = oo.id
							where bo_bb.user_id = OLD.id
							  and oo.company_id = OLD.company_id
						);

						if (count_entries > 0) then
							raise exception 'employee__no_change_when_booking: For the employee, there are bookings or ownerships present, so no change of user or company is possible';
						end if;
						return NEW;
					end;
				$$ language plpgsql;

				create trigger employee__no_change_when_booking
					before update
					on office_employee
					for each row
				execute procedure employee__no_change_when_booking();
			''',
			reverse_sql='''
				drop trigger if exists employee__no_change_when_booking on office_employee;
				
				drop function employee__no_change_when_booking();
			'''
		),
	]