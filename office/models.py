from django.db import models
from django.apps import apps as django_apps
from django.utils.translation import ugettext_lazy as _
import django_helpers.validate_unique
from django.contrib.auth import get_user_model
from django.conf import settings
from fernet_fields import EncryptedCharField
from django.core.exceptions import ValidationError

UserModel = get_user_model()



class Company(models.Model):
	"""Company
	"""

	created = models.DateTimeField(auto_now_add=True)
	modified = models.DateTimeField(auto_now=True)

	name = models.CharField(
		max_length= 128,
		unique= True,
	)

	slack_token = EncryptedCharField(
		max_length= 1024,
		help_text= 'Encrypted storage in database. Access token to Slack workspace of company.',
	)

	def __str__(self):
		return f'({self.id}) {self.name}'



class Employee(models.Model):
	"""
	Users, able to book within a company. Also, the users slack workspace assignment
	"""

	created = models.DateTimeField(auto_now_add=True)
	modified = models.DateTimeField(auto_now=True)

	company = models.ForeignKey(
		Company,
		on_delete= models.CASCADE,
		#related_name= 'company',
	)
	user = models.OneToOneField(
		UserModel,
		on_delete= models.CASCADE,
		#related_name= 'user',
	)

	slack = models.CharField(_('slack name'), max_length=64)

	class Meta:
		unique_together = ['company', 'slack', ]


	def clean(self):
		super(Employee, self).clean()
		if not all( [getattr(self, _field, None) for _field in ('company','user','slack')] ):
			return

		# no_change_of_company_or_user_if_ownership_or_booking_entry
		if self.pk:
			# Get previous company entry
			prev_entry = self.__class__.objects.get(pk=self.pk)
			if prev_entry.company_id != self.company_id \
					or prev_entry.user_id != self.user_id:

				# Check, if any set booking or ownership
				OwnershipModel = django_apps.get_model(settings.OFFICE.OWNERSHIP_MODEL, require_ready= False)
				BookingModel = django_apps.get_model(settings.OFFICE.BOOKING_MODEL, require_ready= False)

				cnt = BookingModel.objects.filter(
					user_id= self.id,
				).count()
				cnt += OwnershipModel.objects.filter(
					user_id= self.id,
				).count()

				if cnt>0:
					raise ValidationError('Changing the company for user is not allowed since there are made bookings or existing ownerships')


	def __str__(self):
		return f'({self.id}) {self.company.name}, {self.user.username}'



class Office(models.Model):
	"""A building, floor or part of a company
	"""

	created = models.DateTimeField(auto_now_add=True)
	modified = models.DateTimeField(auto_now=True)

	company = models.ForeignKey(
		Company,
		on_delete= models.CASCADE,
		#related_name= 'company',
	)
	name = models.CharField(
		max_length= 128,
	)

	class Meta:
		unique_together = ['company', 'name']

	def __str__(self):
		return f'({self.id}) {self.name} [Company ID {self.company_id}]'



class Room(models.Model):
	"""Room within a office (building, floor or part of a company)
	"""

	created = models.DateTimeField(auto_now_add=True)
	modified = models.DateTimeField(auto_now=True)

	office = models.ForeignKey(
		Office,
		on_delete= models.CASCADE,
		#related_name= 'office',
	)
	name = models.CharField(
		max_length= 128,
	)

	class Meta:
		unique_together = ['office', 'name']

	def __str__(self):
		return f'({self.id}) {self.name} [Office ID {self.office_id}]'



class Desk(models.Model):
	"""Desk or other utility to book within a room
	"""

	created = models.DateTimeField(auto_now_add=True)
	modified = models.DateTimeField(auto_now=True)

	room = models.ForeignKey(
		Room,
		on_delete= models.CASCADE,
		#related_name= 'room',
	)
	name = models.CharField(
		max_length= 128,
	)

	devices = models.ManyToManyField(
		'office.Hardware',
		through= 'office.Device',
		help_text= _('hardware devices, the desk is equipped with')
	)

	class Meta:
		unique_together = ['room', 'name']

	def __str__(self):
		return f'({self.id}) {self.name} [Room ID {self.room_id}]'



class Hardware(models.Model):
	"""Hardware, used in the company relevant when booking a desk
	e.g. Type of docking station, monitors, ...

	Do not confuse with device, the instance of the Hardware.
	"""

	created = models.DateTimeField(auto_now_add=True)
	modified = models.DateTimeField(auto_now=True)

	type = models.CharField(
		max_length= 64,
		help_text= _('e.g. monitor, docking station, ...')
	)
	brand = models.CharField(
		max_length= 64,
	)
	model = models.CharField(
		max_length= 64,
		help_text= _('unique model entitlement from producer')
	)
	main_characteristic = models.TextField(
		help_text= _('e.g. for reason of compatibility or claim when booking desk')
	)
	details = models.TextField(
		null= True,
		blank= True,
		help_text= _('Further characteristics of the hardware, relevant when booking desk')
	)

	class Meta:
		unique_together = ['brand','model']

	def __str__(self):
		return f'({self.id}) {self.type} {self.brand} {self.model}'



class Device(models.Model):
	"""Assignment of Hardware and Desk
	Devices, a desk is equipped with. Assignment of company serial number to the desk.
	"""

	created = models.DateTimeField(auto_now_add=True)
	modified = models.DateTimeField(auto_now=True)

	desk = models.ForeignKey(
		Desk,
		on_delete= models.CASCADE,
		#related_name= 'desk',
	)
	hardware = models.ForeignKey(
		Hardware,
		on_delete= models.CASCADE,
		#related_name= 'hardware',
	)

	company_serial_number = models.CharField(
		max_length= 64,
		help_text= 'Serial number must be unique within the company',
	)


	def validate_unique(self, exclude=None):
		super(Device, self).validate_unique(exclude)

		# Check for unique serial number within the company
		#	Database unique constraint is a trigger, set in migration file 0004
		django_helpers.validate_unique.validate_unique(
			self,
			company_serial_number= self.company_serial_number,
			desk__room__office__company= self.desk.room.office.company,
		)

	def save(self, *args, **kwargs):
		#self.full_clean()
		super(Device, self).save(*args, **kwargs)


	def __str__(self):
		return f'({self.id}) {self.hardware}: {self.company_serial_number} [{self.desk}]'