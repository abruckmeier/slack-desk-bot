from pathlib import Path
import os
from types import SimpleNamespace
from django.utils import timezone



# Set PostgreSQL database datetime field to not storing microseconds, but full seconds
from django.db.backends.postgresql.base import DatabaseWrapper
DatabaseWrapper.data_types['DateTimeField'] = 'timestamptz(0)'



# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent



# Get the personal settings in personal_settings.py
try:
    from .personal_settings import *
except:
    raise Exception("No personal_settings.py found")



# Application definition

INSTALLED_APPS = [
    'booking',
    'office',
    'user',

    'django_db_views',
    'rangefilter',

    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'slack_desk_bot.urls'
#handler404 = 'slack_desk_bot.views.handler404'
#handler500 = 'slack_desk_bot.views.handler500'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'slack_desk_bot.wsgi.application'



# Authentication / Login / User
AUTHENTICATION_BACKENDS = [
    'user.backends.UserBackend',
]
AUTH_USER_MODEL = 'user.User'
LOGIN_REDIRECT_URL = '/'



# Password validation
# https://docs.djangoproject.com/en/3.1/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]



# Internationalization
# https://docs.djangoproject.com/en/3.1/topics/i18n/

LANGUAGE_CODE = 'de'

TIME_ZONE = 'Europe/Berlin'

USE_I18N = True

USE_L10N = True

USE_TZ = True



# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.1/howto/static-files/
STATIC_ROOT = os.path.abspath(os.path.join(BASE_DIR,'static'))
STATIC_URL = '/static/'
STATICFILES_DIRS = [
    os.path.join(BASE_DIR, 'slack_desk_bot', 'static'),
]

# Ablageort der Uploads
MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')



## E-Mail settings (non-secure)
# Switch on/off the E-Mail Notifications. CAUTION: When switched off, important workflows (like registration) can not be conducted!
EMAIL_ACTIVE = True
# There are other backends available for testing: Use `console` instead of `smtp`
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'



### App Settings

## App `booking`
BOOKING = SimpleNamespace(
    # Model of `desk` in `office` app
	DESK_MODEL= 'office.Desk',
    # Model of `employee` or user in `office` app.
    EMPLOYEE_MODEL= 'office.Employee',
)

## App `office`
OFFICE = SimpleNamespace(
    # Module `admin` in app `booking`.
    #   Here, following Inlines are expected to be added to the admin view of model `office.Desk`: `Owner_Inline`, `Release_Inline`, `Booking_Inline`.
    #   If Inlines are not found or field is empty, they will not be included.
	BOOKING_ADMIN= 'booking.admin',
    # Model of ownership for clean method of `Employee'
    OWNERSHIP_MODEL= 'booking.Ownership',
    # Model of booking for clean method of `Employee`
    BOOKING_MODEL= 'booking.Booking',
)

