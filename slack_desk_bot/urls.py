from django.contrib import admin
from django.views.generic import RedirectView
from django.urls import path
from django.conf.urls import url

urlpatterns = [
    path('administration/', admin.site.urls, name='administration'),
    url(r'^favicon\.ico$', RedirectView.as_view(url='/static/favicon.ico')),
    url(r'^\_favicon\.ico$', RedirectView.as_view(url='/static/favicon.ico')),
]
