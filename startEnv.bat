@echo off

:: Activate the virtual environment
activate slack_desk_bot_env

:: Install the necessary packages (Only once to do)
:: pip install -r requirements.txt