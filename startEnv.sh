#!/bin/bash

# Activate the virtual environment
source /usr/virtualenvs/slack_desk_bot_env/bin/activate

# Install the necessary packages (Only once to do)
#pip install -r requirements.txt