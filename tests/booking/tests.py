from django.test import TestCase
from django.utils import timezone
from django.db import transaction

from tests.data import office_data, booking_data
import booking.models
import django_helpers.tests



# -----------------
#	Tests for Ownership
# -----------------

class TestCase__Ownership__Database_Integrity(TestCase, django_helpers.tests.Test_Setup__Database_Integrity):
	"""
	Tests of database integrity for Table `booking.Ownership`
	"""

	def setUp(self) -> None:
		self.instances = office_data.create_office_data()

		self.user_1 = self.instances.emp_1
		self.user_2 = self.instances.emp_2
		self.desk_1 = self.instances.desk_1_1_2_1
		self.desk_2 = self.instances.desk_2_1_2_1

		self.ref_ts = timezone.now() + timezone.timedelta(minutes=5)
		self.ref_ts = self.ref_ts.replace(microsecond=0)


	def test__set_ts_from__but_no__ts_to(self):
		"""Starting a ownership without `ts_to` shall succeed"""
		inst = self.save(booking.models.Ownership,
			user=self.user_1,
			desk=self.desk_1,
			ts_from=timezone.now() + timezone.timedelta(minutes=1))
		self.assertIsInstance(inst, booking.models.Ownership)

	def test__ts_from__lower_than__ts_to(self):
		"""set a ts_from lower than ts_to, then create ts_to lower than ts_from"""
		inst = self.save(booking.models.Ownership,
			user=self.user_1,
			desk=self.desk_1,
			ts_from=self.ref_ts,
			ts_to=self.ref_ts + timezone.timedelta(minutes=1)
		)
		self.assertIsInstance(inst, booking.models.Ownership)

		with self.assertRaises(self.expected_error_class()):
			self.save(booking.models.Ownership,
				user=self.user_1,
				desk=self.desk_1,
				ts_from=self.ref_ts + timezone.timedelta(minutes=3),
				ts_to=self.ref_ts + timezone.timedelta(minutes=2)
			)

	def test__user_must_end_ownership_before_new(self):
		"""Start new ownership, start a second new ownership for user"""

		inst_1 = self.save(booking.models.Ownership,
			user=self.user_1,
			desk=self.desk_1,
			ts_from=self.ref_ts,
			ts_to=self.ref_ts + timezone.timedelta(minutes=1)
		)
		self.assertIsInstance(inst_1, booking.models.Ownership)

		inst_2 = self.save(booking.models.Ownership,
			user=self.user_1,
			desk=self.desk_1,
			ts_from=self.ref_ts + timezone.timedelta(minutes=2)
		)
		self.assertIsInstance(inst_2, booking.models.Ownership)

		with self.assertRaises(self.expected_error_class()):
			self.save(booking.models.Ownership,
				user=self.user_1,
				desk=self.desk_2,
				ts_from=self.ref_ts + timezone.timedelta(minutes=3)
			)

	def test__desk_ownership_must_end_before_new(self):
		"""Start new ownership, start a second new ownership for desk"""
		inst_1 = self.save(booking.models.Ownership,
			user=self.user_1,
			desk=self.desk_1,
			ts_from=self.ref_ts,
			ts_to=self.ref_ts + timezone.timedelta(minutes=1)
		)
		self.assertIsInstance(inst_1, booking.models.Ownership)

		inst_2 = self.save(booking.models.Ownership,
			user=self.user_2,
			desk=self.desk_1,
			ts_from=self.ref_ts + timezone.timedelta(minutes=2)
		)
		self.assertIsInstance(inst_2, booking.models.Ownership)

		with self.assertRaises(self.expected_error_class()):
			self.save(booking.models.Ownership,
				user=self.user_1,
				desk=self.desk_1,
				ts_from=self.ref_ts + timezone.timedelta(minutes=1)
			)

	def test__exclude_overlapping_user_ownership_of_desk(self):
		"""Test for overlapping ownerships to fail"""
		b_1 = self.save(booking.models.Ownership,
			user=self.user_1,
			desk=self.desk_1,
			ts_from=self.ref_ts,
			ts_to=self.ref_ts + timezone.timedelta(minutes=1)
		)
		self.assertIsInstance(b_1, booking.models.Ownership)

		b_2 = self.save(booking.models.Ownership,
			user=self.user_1,
			desk=self.desk_1,
			ts_from=self.ref_ts + timezone.timedelta(minutes=3),
			ts_to=self.ref_ts + timezone.timedelta(minutes=4)
		)
		self.assertIsInstance(b_2, booking.models.Ownership)

		b_3 = self.save(booking.models.Ownership,
			user=self.user_1,
			desk=self.desk_1,
			ts_from=self.ref_ts + timezone.timedelta(minutes=6),
			ts_to=self.ref_ts + timezone.timedelta(minutes=7)
		)
		self.assertIsInstance(b_3, booking.models.Ownership)

		with self.assertRaises(self.expected_error_class()):
			with transaction.atomic():
				self.save(booking.models.Ownership,
					user=self.user_2,
					desk=self.desk_1,
					ts_from=self.ref_ts + timezone.timedelta(minutes=2),
					ts_to=self.ref_ts + timezone.timedelta(minutes=5)
				)
		with self.assertRaises(self.expected_error_class()):
			with transaction.atomic():
				self.save(
					booking.models.Ownership,
					user=self.user_2,
					desk=self.desk_1,
					ts_from=self.ref_ts + timezone.timedelta(minutes=3),
					ts_to=self.ref_ts + timezone.timedelta(minutes=5)
				)
		with self.assertRaises(self.expected_error_class()):
			with transaction.atomic():
				self.save(booking.models.Ownership,
					user=self.user_2,
					desk=self.desk_1,
					ts_from=self.ref_ts + timezone.timedelta(minutes=2),
					ts_to=self.ref_ts + timezone.timedelta(minutes=4)
				)
		with self.assertRaises(self.expected_error_class()):
			with transaction.atomic():
				self.save(booking.models.Ownership,
					user=self.user_2,
					desk=self.desk_1,
					ts_from=self.ref_ts,
					ts_to=self.ref_ts + timezone.timedelta(minutes=4)
				)

		b_4 = self.save(booking.models.Ownership,
			user=self.user_2,
			desk=self.desk_1,
			ts_from=self.ref_ts + timezone.timedelta(minutes=1),
			ts_to=self.ref_ts + timezone.timedelta(minutes=2)
		)
		self.assertIsInstance(b_4, booking.models.Ownership)

	def test__exclude_overlapping_user_ownership(self):
		"""Test for overlapping ownerships to fail"""
		b_1 = self.save(booking.models.Ownership,
			user=self.user_1,
			desk=self.desk_1,
			ts_from=self.ref_ts,
			ts_to=self.ref_ts + timezone.timedelta(minutes=1)
		)
		self.assertIsInstance(b_1, booking.models.Ownership)

		b_2 = self.save(booking.models.Ownership,
			user=self.user_1,
			desk=self.desk_1,
			ts_from=self.ref_ts + timezone.timedelta(minutes=3),
			ts_to=self.ref_ts + timezone.timedelta(minutes=4)
		)
		self.assertIsInstance(b_2, booking.models.Ownership)

		b_3 = self.save(booking.models.Ownership,
			user=self.user_1,
			desk=self.desk_1,
			ts_from=self.ref_ts + timezone.timedelta(minutes=6),
			ts_to=self.ref_ts + timezone.timedelta(minutes=7)
		)
		self.assertIsInstance(b_3, booking.models.Ownership)

		with self.assertRaises(self.expected_error_class()):
			with transaction.atomic():
				self.save(booking.models.Ownership,
					user=self.user_1,
					desk=self.desk_2,
					ts_from=self.ref_ts + timezone.timedelta(minutes=2),
					ts_to=self.ref_ts + timezone.timedelta(minutes=5)
				)
		with self.assertRaises(self.expected_error_class()):
			with transaction.atomic():
				self.save(booking.models.Ownership,
					user=self.user_1,
					desk=self.desk_2,
					ts_from=self.ref_ts + timezone.timedelta(minutes=2),
					ts_to=self.ref_ts + timezone.timedelta(minutes=5)
				)
		with self.assertRaises(self.expected_error_class()):
			with transaction.atomic():
				self.save(booking.models.Ownership,
					user=self.user_1,
					desk=self.desk_2,
					ts_from=self.ref_ts + timezone.timedelta(minutes=2),
					ts_to=self.ref_ts + timezone.timedelta(minutes=4)
				)
		with self.assertRaises(self.expected_error_class()):
			with transaction.atomic():
				self.save(booking.models.Ownership,
					user=self.user_1,
					desk=self.desk_2,
					ts_from=self.ref_ts,
					ts_to=self.ref_ts + timezone.timedelta(minutes=4)
				)

		b_4 = self.save(booking.models.Ownership,
			user=self.user_1,
			desk=self.desk_2,
			ts_from=self.ref_ts + timezone.timedelta(minutes=1),
			ts_to=self.ref_ts + timezone.timedelta(minutes=2)
		)
		self.assertIsInstance(b_4, booking.models.Ownership)

	def test__modify_existing_entry(self):
		"""Test for modifying existing entry shall not fail"""
		b_1 = self.save(booking.models.Ownership,
			user=self.user_1,
			desk=self.desk_1,
			ts_from=self.ref_ts,
			ts_to=self.ref_ts + timezone.timedelta(minutes=1)
		)
		self.assertIsInstance(b_1, booking.models.Ownership)

		b_1.ts_to = self.ref_ts + timezone.timedelta(minutes=2)
		self.update(b_1)
		self.assertIsInstance(b_1, booking.models.Ownership)

	def test__desk_is_in_company_of_employee(self):
		"""Check, if a ownership or booking fails, when desk, not in company of employee is booked"""
		with self.assertRaises(self.expected_error_class()):
			self.save(
				booking.models.Ownership,
				user= self.instances.emp_3,
				desk= self.instances.desk_1_1_2_1,
				ts_from= self.ref_ts,
				ts_to= self.ref_ts + timezone.timedelta(minutes=1),
			)



class TestCase__Ownership__Django_Validation(django_helpers.tests.Test_Setup__Django_Validation,TestCase__Ownership__Database_Integrity):
	"""
	Tests of django validation for model `booking.Ownership`.
	Same tests as for database Integrity. This should be the case due to expected same error alerts
	"""
	pass



# -----------------
#	Tests for Release
# -----------------

class TestCase__Release__Database_Integrity(TestCase, django_helpers.tests.Test_Setup__Database_Integrity):
	"""
	Tests of database integrity for Table `booking.Release`
	"""

	def setUp(self) -> None:
		self.instances = booking_data.create_booking_data()

	def test__ts_from__lower_than__ts_to(self):
		"""set a ts_from lower than ts_to, then change ts_to to lower than ts_from"""
		ref_ts = self.instances.os_1.ts_from
		inst = self.save(booking.models.Release,
			ownership= self.instances.os_1,
			ts_from=ref_ts + timezone.timedelta(minutes=10),
			ts_to=ref_ts + timezone.timedelta(minutes=11)
		)
		self.assertIsInstance(inst, booking.models.Release)
		with self.assertRaises(self.expected_error_class()):
			self.save(booking.models.Release,
				ownership= self.instances.os_1,
				ts_from=ref_ts + timezone.timedelta(minutes=3),
				ts_to=ref_ts + timezone.timedelta(minutes=2)
			)

	def test__exclude_overlapping_timerange(self):
		"""Test for overlapping releases to fail"""
		ref_ts = self.instances.os_1.ts_from
		b_1 = self.save(booking.models.Release,
			ownership= self.instances.os_1,
			ts_from=ref_ts,
			ts_to=ref_ts + timezone.timedelta(minutes=1)
		)
		self.assertIsInstance(b_1, booking.models.Release)

		b_2 = self.save(booking.models.Release,
			ownership= self.instances.os_1,
			ts_from=ref_ts + timezone.timedelta(minutes=3),
			ts_to=ref_ts + timezone.timedelta(minutes=4)
		)
		self.assertIsInstance(b_2, booking.models.Release)

		b_3 = self.save(booking.models.Release,
			ownership= self.instances.os_1,
			ts_from=ref_ts + timezone.timedelta(minutes=6),
			ts_to=ref_ts + timezone.timedelta(minutes=7)
		)
		self.assertIsInstance(b_3, booking.models.Release)

		with self.assertRaises(self.expected_error_class()):
			with transaction.atomic():
				self.save(booking.models.Release,
					ownership= self.instances.os_1,
					ts_from=ref_ts + timezone.timedelta(minutes=2),
					ts_to=ref_ts + timezone.timedelta(minutes=5)
				)
		with self.assertRaises(self.expected_error_class()):
			with transaction.atomic():
				self.save(booking.models.Release,
					ownership= self.instances.os_1,
					ts_from=ref_ts + timezone.timedelta(minutes=3), ts_to=ref_ts + timezone.timedelta(minutes=5)
				)
		with self.assertRaises(self.expected_error_class()):
			with transaction.atomic():
				self.save(booking.models.Release,
					ownership= self.instances.os_1,
					ts_from=ref_ts + timezone.timedelta(minutes=2),
					ts_to=ref_ts + timezone.timedelta(minutes=4)
				)
		with self.assertRaises(self.expected_error_class()):
			with transaction.atomic():
				self.save(booking.models.Release,
					ownership= self.instances.os_1,
					ts_from=ref_ts,
					ts_to=ref_ts + timezone.timedelta(minutes=4)
				)

		b_4 = self.save(booking.models.Release,
			ownership= self.instances.os_1,
			ts_from=ref_ts + timezone.timedelta(minutes=1),
			ts_to=ref_ts + timezone.timedelta(minutes=2)
		)
		self.assertIsInstance(b_4, booking.models.Release)

	def test__modify_existing_entry(self):
		"""Test for modifying existing entry shall not fail"""
		ref_ts = self.instances.os_1.ts_from
		b_1 = self.save(booking.models.Release,
			ownership=self.instances.os_1,
			ts_from=ref_ts,
			ts_to=ref_ts + timezone.timedelta(minutes=1)
		)
		self.assertIsInstance(b_1, booking.models.Release)

		b_1.ts_to = ref_ts + timezone.timedelta(minutes=2)
		self.update(b_1)
		self.assertIsInstance(b_1, booking.models.Release)

	def test__ts_from__gte__ownership_start(self):
		"""ts_from of release shall not be lower than ownership start"""
		ref_ts = self.instances.os_1.ts_from
		inst = self.save(booking.models.Release,
			ownership= self.instances.os_1,
			ts_from=ref_ts + timezone.timedelta(minutes=1),
			ts_to=ref_ts + timezone.timedelta(minutes=11)
		)
		self.assertIsInstance(inst, booking.models.Release)

		inst.ts_from = ref_ts - timezone.timedelta(minutes=1)
		with self.assertRaises(self.expected_error_class()):
			self.update(inst)

	def test__ts_to__lte__ownership_end(self):
		"""ts_to of release shall not be greater than ownership end"""
		ref_ts = self.instances.os_1.ts_from
		inst = self.save(booking.models.Release,
			ownership= self.instances.os_1,
			ts_from=ref_ts + timezone.timedelta(minutes=1),
			ts_to=self.instances.os_1.ts_to,
		)
		self.assertIsInstance(inst, booking.models.Release)

		inst.ts_to = inst.ts_to + timezone.timedelta(minutes=1)
		with self.assertRaises(self.expected_error_class()):
			with transaction.atomic():
				self.update(inst)

		inst_2 = self.save(booking.models.Release,
			ownership= self.instances.os_3,
			ts_from= self.instances.os_3.ts_from,
			ts_to= self.instances.os_3.ts_from + timezone.timedelta(days=30),
		)
		self.assertIsInstance(inst_2, booking.models.Release)



class TestCase__Release__Django_Validation(django_helpers.tests.Test_Setup__Django_Validation,TestCase__Release__Database_Integrity):
	"""
	Tests of django validation for model `booking.Release`.
	"""
	pass



# -----------------
#	Tests for Changes in Ownership with existing Release data
# -----------------

class TestCase__Ownership_with_Release_Data__Database_Integrity(TestCase, django_helpers.tests.Test_Setup__Database_Integrity):
	"""
	Tests of database integrity for Table `booking.Ownership` with existing Release values of ownership
	"""

	def setUp(self) -> None:
		self.instances = booking_data.create_booking_data(with_release_data=True)

	def test__ownership_ts_from__lte__min_release_start(self):
		"""When updating ownership time range, consider release entries to be still correct"""
		self.instances.os_1.ts_from += timezone.timedelta(hours=1)
		self.update(self.instances.os_1)

		self.instances.os_1.ts_from += timezone.timedelta(minutes=1)
		with self.assertRaises(self.expected_error_class()):
			self.update(self.instances.os_1)

	def test__ownership_ts_to__gte__max_release_end(self):
		"""When updating ownership time range, consider release entries to be still correct"""
		self.instances.os_1.ts_to -= timezone.timedelta(hours=1)
		self.update(self.instances.os_1)

		self.instances.os_1.ts_to -= timezone.timedelta(minutes=1)
		with self.assertRaises(self.expected_error_class()):
			with transaction.atomic():
				self.update(self.instances.os_1)

		self.instances.os_3.ts_to = self.instances.os_3.ts_from + timezone.timedelta(hours=6)
		with self.assertRaises(self.expected_error_class()):
			with transaction.atomic():
				self.update(self.instances.os_3)

		self.instances.os_3.ts_to = self.instances.os_3.ts_from + timezone.timedelta(days=1)
		self.update(self.instances.os_3)



class TestCase__Ownership_with_Release_Data__Django_Validation(django_helpers.tests.Test_Setup__Django_Validation,TestCase__Ownership_with_Release_Data__Database_Integrity):
	"""
	Tests of django validation for model `booking.Release`.
	"""
	pass



# -----------------
#	Tests for Booking and changes in Ownership and Release data
# -----------------

class TestCase__Booking__Database_Integrity(TestCase, django_helpers.tests.Test_Setup__Database_Integrity):
	"""
	Tests of database integrity for Table `booking.Booking`.
	Some tests are not necessary due to usage of base class of ownership.
	"""

	def setUp(self) -> None:
		self.instances = booking_data.create_booking_data(with_release_data=True)
		self.ref_ts = self.instances.os_1.ts_from

	def test__check_for_availability(self):
		"""Perform bookings in ownership (error), in release and non-ownership"""
		inst = self.save(
			booking.models.Booking,
			user=self.instances.emp_2,
			desk=self.instances.desk_1_1_2_1,
			ts_from=self.ref_ts + timezone.timedelta(hours=2),
			ts_to=self.ref_ts + timezone.timedelta(hours=3),
		)
		self.assertIsInstance(inst, booking.models.Booking)

		inst = self.save(
			booking.models.Booking,
			user=self.instances.emp_1,
			desk=self.instances.desk_1_1_2_1,
			ts_from=self.ref_ts + timezone.timedelta(hours=9, minutes=30),
			ts_to=self.ref_ts + timezone.timedelta(hours=10, minutes=30),
		)
		self.assertIsInstance(inst, booking.models.Booking)

		with self.assertRaises(self.expected_error_class()):
			self.save(
				booking.models.Booking,
				user=self.instances.emp_2,
				desk=self.instances.desk_1_1_2_1,
				ts_from=self.ref_ts,
				ts_to=self.ref_ts + timezone.timedelta(hours=1),
			)

	def test__change_release_date_in_closed_ownership(self):
		"""Make a booking within release and try reducing the release time range"""
		made_booking = self.save(
			booking.models.Booking,
			user=self.instances.emp_2,
			desk=self.instances.desk_1_1_2_1,
			ts_from=self.ref_ts + timezone.timedelta(hours=2),
			ts_to=self.ref_ts + timezone.timedelta(hours=3),
		)
		self.assertIsInstance(made_booking, booking.models.Booking)

		self.instances.r_1.ts_from = self.instances.r_1.ts_from + timezone.timedelta(hours=1)
		self.update(self.instances.r_1)

		self.instances.r_1.ts_from = self.instances.r_1.ts_from + timezone.timedelta(hours=1)
		with self.assertRaises(self.expected_error_class()):
			with transaction.atomic():
				self.update(self.instances.r_1)
		self.instances.r_1.ts_from = self.instances.r_1.ts_from - timezone.timedelta(hours=1)

		self.instances.r_1.ts_to = self.instances.r_1.ts_to - timezone.timedelta(hours=1)
		self.update(self.instances.r_1)

		self.instances.r_1.ts_to = self.instances.r_1.ts_to - timezone.timedelta(hours=1)
		with self.assertRaises(self.expected_error_class()):
			with transaction.atomic():
				self.update(self.instances.r_1)

	def test__change_release_date_in_open_ownership(self):
		"""Make a booking within release and try reducing the release time range"""
		made_booking = self.save(
			booking.models.Booking,
			user=self.instances.emp_2,
			desk=self.instances.desk_1_1_2_1,
			ts_from=self.ref_ts + timezone.timedelta(hours=17),
			ts_to=self.ref_ts + timezone.timedelta(hours=18),
		)
		self.assertIsInstance(made_booking, booking.models.Booking)

		self.instances.r_3.ts_from = self.instances.r_3.ts_from + timezone.timedelta(hours=1)
		self.update(self.instances.r_3)

		self.instances.r_3.ts_from = self.instances.r_3.ts_from + timezone.timedelta(hours=1)
		with self.assertRaises(self.expected_error_class()):
			with transaction.atomic():
				self.update(self.instances.r_3)
		self.instances.r_3.ts_from = self.instances.r_3.ts_from - timezone.timedelta(hours=1)

		self.instances.r_3.ts_to = self.instances.r_3.ts_to - timezone.timedelta(hours=1)
		self.update(self.instances.r_3)

		self.instances.r_3.ts_to = self.instances.r_3.ts_to - timezone.timedelta(hours=1)
		with self.assertRaises(self.expected_error_class()):
			with transaction.atomic():
				self.update(self.instances.r_3)

	def test__change_ownership_date(self):
		"""Modify the ownerships around a booking between two ownerships"""
		made_booking = self.save(
			booking.models.Booking,
			user=self.instances.emp_1,
			desk=self.instances.desk_1_1_2_1,
			ts_from=self.ref_ts + timezone.timedelta(hours=9),
			ts_to=self.ref_ts + timezone.timedelta(hours=10),
		)
		self.assertIsInstance(made_booking, booking.models.Booking)

		self.instances.os_2.ts_to = self.instances.os_2.ts_to + timezone.timedelta(hours=1)
		self.update(self.instances.os_2)

		self.instances.os_2.ts_to = self.instances.os_2.ts_to + timezone.timedelta(minutes=1)
		with self.assertRaises(self.expected_error_class()):
			with transaction.atomic():
				self.update(self.instances.os_2)
		self.instances.os_2.ts_to = self.instances.os_2.ts_to - timezone.timedelta(minutes=1)

		self.instances.os_3.ts_from -= timezone.timedelta(hours=1)
		self.update(self.instances.os_3)

		self.instances.os_3.ts_from -= timezone.timedelta(minutes=1)
		with self.assertRaises(self.expected_error_class()):
			with transaction.atomic():
				self.update(self.instances.os_3)



class TestCase__Booking__Django_Validation(django_helpers.tests.Test_Setup__Django_Validation, TestCase__Booking__Database_Integrity):
	"""
	Tests of django validation for model `booking.Booking`.
	"""
	pass

