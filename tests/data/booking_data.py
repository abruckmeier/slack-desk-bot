#!/usr/bin/env python3

from pathlib import Path
import os
import sys
import django

if __name__ == '__main__':
	# Setup the Django environment
	BASE_DIR = Path(__file__).resolve().parent.parent.parent
	sys.path.append(str(BASE_DIR))
	os.environ.setdefault("DJANGO_SETTINGS_MODULE", "slack_desk_bot.settings")
	django.setup()


from django.apps import apps as django_apps
from django.db import transaction
from django.conf import settings
from django.utils import timezone
import booking.models
import tests.data.office_data
from types import SimpleNamespace


DeskModel = django_apps.get_model(settings.BOOKING.DESK_MODEL, require_ready= False)


@transaction.atomic
def create_booking_data(with_release_data=False):
	"""
	Create desk data, user data and booking data. Return instances of created data.
	Office data and user data are loaded in advance.

	:param with_release_data: Boolean, opional, default: False. If True, create some Release data of ownerships.
	:return: types.SimpleNamespace instance. Instances of created data.
	"""

	instances = tests.data.office_data.create_office_data() # This loads user_data, too.

	ref_ts = timezone.now()
	ref_ts = ref_ts.replace(microsecond=0)
	instances.ref_ts = ref_ts


	instances.os_1 = booking.models.Ownership.objects.create(
		user= instances.emp_1,
		desk= instances.desk_1_1_2_1,
		ts_from=ref_ts,
		ts_to=ref_ts + timezone.timedelta(hours=5),
	)
	instances.os_2 = booking.models.Ownership.objects.create(
		user= instances.emp_2,
	 	desk= instances.desk_1_1_2_1,
		ts_from=ref_ts + timezone.timedelta(hours=5),
		ts_to=ref_ts + timezone.timedelta(hours=8),
	)
	instances.os_3 = booking.models.Ownership.objects.create(
		user= instances.emp_1,
		desk= instances.desk_1_1_2_1,
		ts_from=ref_ts + timezone.timedelta(hours=11),
	)
	instances.os_4 = booking.models.Ownership.objects.create(
		user= instances.emp_3,
	 	desk= instances.desk_1_1_1_2,
		ts_from=ref_ts + timezone.timedelta(hours=6),
		ts_to=ref_ts + timezone.timedelta(hours=9),
	)
	instances.os_5 = booking.models.Ownership.objects.create(
		user= instances.emp_3,
	 	desk= instances.desk_1_1_1_2,
		ts_from=ref_ts + timezone.timedelta(hours=12),
		ts_to=ref_ts + timezone.timedelta(hours=15),
	)

	if with_release_data:
		instances.r_1 = booking.models.Release.objects.create(
			ownership= instances.os_1,
			ts_from= ref_ts + timezone.timedelta(hours=1),
			ts_to= ref_ts + timezone.timedelta(hours=4),
		)
		instances.r_3 = booking.models.Release.objects.create(
			ownership= instances.os_3,
			ts_from= ref_ts + timezone.timedelta(hours=16),
			ts_to= ref_ts + timezone.timedelta(hours=19),
		)
		instances.r_4 = booking.models.Release.objects.create(
			ownership= instances.os_4,
			ts_from= ref_ts + timezone.timedelta(hours=6, minutes=30),
			ts_to= ref_ts + timezone.timedelta(hours=7),
		)
		instances.r_5 = booking.models.Release.objects.create(
			ownership= instances.os_5,
			ts_from= ref_ts + timezone.timedelta(hours=13),
			ts_to= ref_ts + timezone.timedelta(hours=14),
		)

	return instances




# Run the Script
if __name__ == '__main__':

	create_booking_data(with_release_data=True)