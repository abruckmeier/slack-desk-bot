#!/usr/bin/env python3

from pathlib import Path
import os
import sys
import django

if __name__ == '__main__':
	# Setup the Django environment
	BASE_DIR = Path(__file__).resolve().parent.parent.parent
	sys.path.append(str(BASE_DIR))
	os.environ.setdefault("DJANGO_SETTINGS_MODULE", "slack_desk_bot.settings")
	django.setup()


from django.db import transaction
import office.models
import tests.data.user_data


@transaction.atomic
def create_office_data():
	"""
	Create office data. Loads user_data at the beginning

	:return: types.SimpleNamespace instance. Instances of created data.
	"""

	instances = tests.data.user_data.create_user_data()

	instances.comp_1 = office.models.Company.objects.create(
		name= 'Company 1',
	)
	instances.off_1_1 = office.models.Office.objects.create(
		company= instances.comp_1,
		name= 'Office 1',
	)
	instances.off_2_1 = office.models.Office.objects.create(
		company= instances.comp_1,
		name= 'Office 2',
	)
	instances.room_1_1_1 = office.models.Room.objects.create(
		office= instances.off_1_1,
		name= 'Room 1',
	)
	instances.room_1_2_1 = office.models.Room.objects.create(
		office= instances.off_2_1,
		name= 'Room 1',
	)
	instances.room_2_2_1 = office.models.Room.objects.create(
		office= instances.off_2_1,
		name= 'Room 2',
	)
	instances.desk_1_1_2_1 = office.models.Desk.objects.create(
		room= instances.room_1_2_1,
		name= 'Desk 1',
	)
	instances.desk_2_1_2_1 = office.models.Desk.objects.create(
		room= instances.room_1_2_1,
		name= 'Desk 2',
	)
	instances.desk_meeting = office.models.Desk.objects.create(
		room= instances.room_2_2_1,
		name= 'Meeting Room 221',
	)


	instances.comp_2 = office.models.Company.objects.create(
		name= 'Company 2',
	)
	instances.off_1_2 = office.models.Office.objects.create(
		company= instances.comp_2,
		name= 'Office 1',
	)
	instances.room_1_1_2 = office.models.Room.objects.create(
		office= instances.off_1_2,
		name= 'Room 1',
	)
	instances.desk_1_1_1_2 = office.models.Desk.objects.create(
		room= instances.room_1_1_2,
		name= 'Desk 1',
	)


	instances.emp_1 = office.models.Employee.objects.create(
		company= instances.comp_1,
		user= instances.user_1,
		slack= instances.user_1.username,
	)
	instances.emp_2 = office.models.Employee.objects.create(
		company= instances.comp_1,
		user= instances.user_2,
		slack= instances.user_2.username,
	)
	instances.emp_3 = office.models.Employee.objects.create(
		company= instances.comp_2,
		user= instances.user_3,
		slack= instances.user_3.username,
	)


	instances.monitor = office.models.Hardware.objects.create(
		type= 'Monitor',
		brand= 'Dell',
		model= 'ABx-c',
		main_characteristic= 'Size: 27 inch',
		details= 'HDMI Plug, ...',
	)
	instances.docking_station = office.models.Hardware.objects.create(
		type= 'Docking Station',
		brand= 'Dell',
		model= 'zb-x',
		main_characteristic= 'USB-C connection',
		details= '',
	)
	instances.beamer = office.models.Hardware.objects.create(
		type= 'Beamer',
		brand= 'Samsung',
		model= 'B-123',
		main_characteristic= 'USB connection',
		details= 'resolution: 600x800px,',
	)


	instances.dev_1 = office.models.Device.objects.create(
		desk= instances.desk_1_1_2_1,
		hardware= instances.monitor,
		company_serial_number= 'sn 123',
	)
	instances.dev_2 = office.models.Device.objects.create(
		desk= instances.desk_1_1_2_1,
		hardware= instances.docking_station,
		company_serial_number= 'sn 124',
	)
	instances.dev_3 = office.models.Device.objects.create(
		desk= instances.desk_meeting,
		hardware= instances.beamer,
		company_serial_number= 'sn 125',
	)
	instances.dev_4 = office.models.Device.objects.create(
		desk= instances.desk_1_1_1_2,
		hardware= instances.monitor,
		company_serial_number= 'sn 123',
	)
	instances.dev_5 = office.models.Device.objects.create(
		desk= instances.desk_1_1_1_2,
		hardware= instances.monitor,
		company_serial_number= 'sn 124',
	)


	return instances




# Run the Script
if __name__ == '__main__':

	instances = create_office_data()