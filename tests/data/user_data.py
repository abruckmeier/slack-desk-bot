#!/usr/bin/env python3

from pathlib import Path
import os
import sys
import django

if __name__ == '__main__':
	# Setup the Django environment
	BASE_DIR = Path(__file__).resolve().parent.parent.parent
	sys.path.append(str(BASE_DIR))
	os.environ.setdefault("DJANGO_SETTINGS_MODULE", "slack_desk_bot.settings")
	django.setup()


from django.db import transaction
from types import SimpleNamespace
import user.models



@transaction.atomic
def create_user_data():
	"""
	Create user data. Three users and one admin user. Return instances of created data.

	:return: types.SimpleNamespace instance. Instances of created data.
	"""

	instances = SimpleNamespace()

	instances.user_1 = user.models.User.objects.create(
		username= 'user_1',
		email= 'user_1@mail.org',
	)
	instances.user_2 = user.models.User.objects.create(
		username= 'user_2',
		email= 'user_2@mail.org',
	)
	instances.user_3 = user.models.User.objects.create(
		username= 'user_3',
		email= 'user_3@mail.org',
	)
	instances.admin = user.models.User.objects.create(
		username= 'admin',
		email= 'admin@mail.org',
		is_superuser= True,
		admin_slack= 'admin',
	)

	return instances




# Run the Script
if __name__ == '__main__':

	instances = create_user_data()