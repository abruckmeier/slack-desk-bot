from django.test import TestCase

from tests.data import office_data, booking_data
import office.models
import django_helpers.tests


# -----------------
#	Tests for Device and its clean/validate method
# -----------------
class TestCase__Device__validate_unique__Database_Integrity(TestCase, django_helpers.tests.Test_Setup__Database_Integrity):
	"""
	Tests for database integrity for table `office.Device`
	"""

	def setUp(self) -> None:
		office_data.create_office_data()
		self.desk_in_company_1 = office.models.Desk.objects.filter(
			room__office__company__name= 'Company 1',
		).first()
		self.desk_in_company_2 = office.models.Desk.objects.filter(
			room__office__company__name= 'Company 2',
		).first()
		self.hardware = office.models.Hardware.objects.all().first()

	def test__same_company_different_sn(self):
		"""Different serial number should be saved"""
		inst = self.save(office.models.Device,
			desk= self.desk_in_company_1,
			hardware= self.hardware,
			company_serial_number= 'sn 999',
		)
		self.assertIsInstance(inst, office.models.Device)

	def test__different_company_same_sn(self):
		"""Same serial number in other company should be saved"""
		inst = self.save(office.models.Device,
			desk= self.desk_in_company_2,
			hardware= self.hardware,
			company_serial_number= 'sn 125',
		)
		self.assertIsInstance(inst, office.models.Device)

	def test__same_company_same_sn(self):
		"""Same serial number should not be saved"""
		with self.assertRaises(self.expected_error_class()):
			self.save(office.models.Device,
				desk= self.desk_in_company_1,
				hardware= self.hardware,
				company_serial_number= 'sn 123',
			)



class TestCase__Device__validate_unique__Django_Validation(django_helpers.tests.Test_Setup__Django_Validation,TestCase__Device__validate_unique__Database_Integrity):
	"""
	Tests for django validation for table `office.Device`.
	Same tests as for database Integrity. This should be the case due to expected same error alerts
	"""
	pass



class TestCase__Employee__clean__Database_Integrity(TestCase, django_helpers.tests.Test_Setup__Database_Integrity):
	"""
	Tests for database integrity for table `office.Employee`
	"""

	def setUp(self) -> None:
		self.instances = booking_data.create_booking_data(with_release_data=True)

	def test__no_change_of_company_or_user_if_ownership_or_booking_entry(self):
		"""Changing the employee entry should fail if booking data is present"""
		self.instances.emp_3.slack = 'changed'
		self.update(self.instances.emp_3)

		self.instances.emp_3.company = self.instances.comp_1
		with self.assertRaises(self.expected_error_class()):
			self.update(self.instances.emp_3)



class TestCase__Employee__clean__Django_Validation(django_helpers.tests.Test_Setup__Django_Validation,TestCase__Employee__clean__Database_Integrity):
	"""
	Tests for Django validation for table `office.Employee`
	"""
	pass