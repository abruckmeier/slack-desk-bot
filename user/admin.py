from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserChangeForm
from django.utils.translation import ugettext_lazy as _

from . import models




# Base Form for the MyUserAdmin
class MyUserChangeForm(UserChangeForm):
	class Meta(UserChangeForm.Meta):
		model = models.User



# Modified UserAdmin with additional fieldsets to adjust in the admin area
class MyUserAdmin(UserAdmin):
	form = MyUserChangeForm
	ordering = ['-pk']
	list_display = ('pk', 'username', 'email', 'is_active', 'is_staff', 'is_superuser')

	# New User Form in Admin area
	add_fieldsets = (
		(None, {
			'classes': ('wide',),
			'fields': ('username','email','password1','password2')
		}),
	)

	# Add Verification and Approvement variables for view and modification
	fieldsets = (
		(None, {'fields': ('username', 'password')}),
		(_('Personal Info'), {'fields': ('first_name', 'last_name', 'email', 'admin_slack')}),
		(_('Permissions'), {'fields': ('is_active','is_staff','is_superuser','groups')}),
		(_('Important dates'), {'fields': ('last_login', 'date_joined')}),
	)



# Register your models here.
admin.site.register(models.User, MyUserAdmin)