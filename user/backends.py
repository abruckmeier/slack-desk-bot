from django.contrib.auth import get_user_model
from django.contrib.auth.backends import BaseBackend



## Login Backend for the users. Allow email as well
# 	If no user is found, try the email address
# 	In settings, to be registered under AUTHENTICATION_BACKENDS
class UserBackend(BaseBackend):

	def authenticate(self, request, username=None, password=None):
		UserModel = get_user_model()
		try:
			user = UserModel.objects.get(username__iexact=username)
		except UserModel.DoesNotExist:
			try:
				user = UserModel.objects.get(email__iexact=username)
			except UserModel.DoesNotExist:
				try:
					user = UserModel.objects.get(slack__iexact=username)
				except UserModel.DoesNotExist:
					return None

		if user.check_password(password):
			return user
		return None


	def get_user(selfself, user_id):
		UserModel = get_user_model()
		try:
			return UserModel.objects.get(pk=user_id)
		except User.DoesNotExist:
			return None