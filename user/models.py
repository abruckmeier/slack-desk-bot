from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import AbstractUser



# User model. Filled in at first registration
class User(AbstractUser):

	email = models.EmailField(_('email address'), unique=True)
	admin_slack = models.CharField(
		_('slack name'),
		blank= True,
		null= True,
		max_length= 64,
		unique= True,
		help_text= _('Optional slack name for admins. They will be notified with this name in the workspace, defined in settings.py')
	)

	def __str__(self):
		return f'({self.id}) {self.username}'
